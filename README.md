# README #

PART - 4 of Full-Stack-Web-Development Specialization

Assignments for Muliplatform Mobile App Development with Web Technologies taught by Prof. Jogesh K. Muppala, Hong Kong University on Coursera: Jan 25th - Feb 25th, 2016.


### Topics covered in Week 1 ###
* Introduction to hybrid mobile application development (HMAD)
* Ionic framework 
* Features of the Ionic framework 
* Ionic components and AngularJS

### Tools and Softwares ###

* Gulp
* Bower
* Node
* Ionic

### Who do I talk to? ###

* Vignesh Subramanian


All works presented are original.